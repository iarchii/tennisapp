package xyz.thecodeside.tennisapp.helpers

import java.text.Normalizer


fun String.flattenToAscii(): String {
    var string = this
    val sb = StringBuilder(string.length)
    string = Normalizer.normalize(string, Normalizer.Form.NFD)
    for (c in string.toCharArray()) {
        if (c <= '\u007F') sb.append(c)
    }
    return sb.toString()
}