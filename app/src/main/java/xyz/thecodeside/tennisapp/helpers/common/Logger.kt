package xyz.thecodeside.tennisapp.helpers.common


interface Logger {
    fun logException(throwable: Throwable)
}