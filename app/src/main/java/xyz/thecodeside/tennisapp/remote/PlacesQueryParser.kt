package xyz.thecodeside.tennisapp.remote

import xyz.thecodeside.tennisapp.helpers.flattenToAscii
import javax.inject.Inject


class PlacesQueryParser
@Inject constructor() {
    //TODO better query parsing, this is as least secure as it's possible, zero validation

    val BASE_QUERY = "tennis+warsaw"

    fun parseQuery(query: String?) =
            BASE_QUERY + getQuerySuffix(query)

    private fun getQuerySuffix(query: String?): String {
        return if (!query.isNullOrEmpty()) {
            //!! - because we are sure here that this immutable string is not null
            "+" + query!!.flattenToAscii().trim().replace(" ", "+")
        } else ""
    }

}