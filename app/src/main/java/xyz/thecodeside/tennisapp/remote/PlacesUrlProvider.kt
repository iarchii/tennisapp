package xyz.thecodeside.tennisapp.remote

import android.content.res.Resources
import xyz.thecodeside.tennisapp.R
import javax.inject.Inject

class PlacesUrlProvider
@Inject constructor(
        private val parser: PlacesQueryParser,
        private val resource: Resources
) {

    //TODO I'm not sure if using web api for places is correct

    val RESP_FORMAT = "json"

    val URL = "https://maps.googleapis.com/maps/api/place/textsearch/$RESP_FORMAT?key=${resource.getString(R.string.google_places_web_key)}&query="

    fun get(query: String?) = URL + parser.parseQuery(query)

}