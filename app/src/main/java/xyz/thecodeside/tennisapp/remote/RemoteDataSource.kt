package xyz.thecodeside.tennisapp.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Url
import xyz.thecodeside.tennisapp.model.PlacesResponse

interface RemoteDataSource{

    @GET
    fun getTennisCourts(@Url query: String ) : Single<PlacesResponse>
}

