package xyz.thecodeside.tennisapp

import android.app.Activity
import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import xyz.thecodeside.tennisapp.dagger.BaseSystemModule
import xyz.thecodeside.tennisapp.dagger.DaggerApplicationComponent
import xyz.thecodeside.tennisapp.dagger.DataModule
import xyz.thecodeside.tennisapp.dagger.UtilsModule
import javax.inject.Inject


class TennisApp : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>
    override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidInjector

    override fun onCreate() {
        super.onCreate()


        DaggerApplicationComponent
                .builder()
                .application(this)
                .baseSystem(BaseSystemModule(this))
                .dataModule(DataModule())
                .utilsModule(UtilsModule())
                .build()
                .inject(this)
    }

}