package xyz.thecodeside.tennisapp.dagger

import android.app.Activity
import dagger.Binds
import dagger.Module
import dagger.android.ActivityKey
import dagger.android.AndroidInjector
import dagger.multibindings.IntoMap
import xyz.thecodeside.tennisapp.map.MapActivity
import xyz.thecodeside.tennisapp.map.MapActivityComponent

@Module(subcomponents = arrayOf(MapActivityComponent::class))
abstract class ActivityBindingModule {
    @Binds
    @IntoMap
    @ActivityKey(MapActivity::class)
    internal abstract fun mainActivityInjectorFactory(
            builder: MapActivityComponent.Builder): AndroidInjector.Factory<out Activity>


}