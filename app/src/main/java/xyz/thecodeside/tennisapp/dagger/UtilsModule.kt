package xyz.thecodeside.tennisapp.dagger

import dagger.Module
import dagger.Provides
import xyz.thecodeside.tennisapp.helpers.common.Logger


@Module
class UtilsModule {

    @Provides
    fun provideLogger(): Logger = object : Logger {
        override fun logException(throwable: Throwable) {
            throwable.printStackTrace()
        }
    }

}