package xyz.thecodeside.tennisapp.dagger

import dagger.Module
import dagger.Provides
import xyz.thecodeside.tennisapp.remote.RestApiProvider
import javax.inject.Singleton


@Module
class DataModule {

    @Provides
    @Singleton
    fun provideRemoteRepository() =
            RestApiProvider().provide()

}

