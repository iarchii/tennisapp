package xyz.thecodeside.tennisapp.base

import android.os.Bundle
import android.view.View
import com.evernote.android.state.StateSaver
import com.hannesdorfmann.mosby3.mvp.MvpPresenter
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.MvpLceViewStateActivity


abstract class BaseLceActivity<CV : View, M, V : MvpLceView<M>, P : MvpPresenter<V>> :
        MvpLceViewStateActivity<CV, M, V, P>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies()
        super.onCreate(savedInstanceState)
        StateSaver.restoreInstanceState(this, savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        StateSaver.saveInstanceState(this, outState)
    }

    protected open fun injectDependencies() {

    }
}