package xyz.thecodeside.tennisapp.map

import com.evernote.android.state.State
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView
import xyz.thecodeside.tennisapp.base.RxBasePresenter
import xyz.thecodeside.tennisapp.helpers.common.Logger
import xyz.thecodeside.tennisapp.helpers.common.applyTransformerSingle
import xyz.thecodeside.tennisapp.model.PlacesData
import xyz.thecodeside.tennisapp.model.PlacesResponse
import xyz.thecodeside.tennisapp.model.PlacesResponseStatus.OK
import xyz.thecodeside.tennisapp.model.PlacesResponseStatus.ZERO_RESULTS
import javax.inject.Inject

class MapPresenter @Inject constructor(

        private val getTennisCourts: GetTennisCourtsUseCase,
        private val logger: Logger
) : RxBasePresenter<MapPresenter.View>() {

    interface View : MvpLceView<List<PlacesData>> {
        fun clear()
    }

    val BASE_LAT = 52.237049
    val BASE_LNG = 21.017532

    @State //TODO fix save instance on rotation of device
    var places : ArrayList<PlacesData> = arrayListOf()

    fun loadData(query: String? = null) {

        view?.showLoading(false)
        view?.clear()
        getTennisCourts.get(query)
                .compose(applyTransformerSingle())
                .subscribe({
                    handleResponse(it)
                },{
                    view?.showError(it,false)
                    logger.logException(it)
                })
                .registerInPresenter()
    }

    private fun handleResponse(it: PlacesResponse) {
        when (it.status) {
            OK, ZERO_RESULTS -> {
                view?.setData(it.places ?: listOf())
                view?.showContent()
            }
            else -> {
                val e = PlacesLoadingException("${it.status} = ${it.errorMessage}")
                view?.showError(e, false)
                logger.logException(e)
            }
        }
    }

    fun setData(data: List<PlacesData>?) {
        places.clear()
        data?.let { places.addAll(it) }
    }




}

