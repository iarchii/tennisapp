package xyz.thecodeside.tennisapp.map

import dagger.Subcomponent
import dagger.android.AndroidInjector
import xyz.thecodeside.tennisapp.dagger.scopes.PerActivity


@PerActivity
@Subcomponent(modules = arrayOf())
interface MapActivityComponent : AndroidInjector<MapActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MapActivity>()
}