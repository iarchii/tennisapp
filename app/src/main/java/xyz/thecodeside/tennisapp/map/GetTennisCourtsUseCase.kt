package xyz.thecodeside.tennisapp.map

import io.reactivex.Single
import xyz.thecodeside.tennisapp.model.PlacesResponse
import xyz.thecodeside.tennisapp.remote.PlacesUrlProvider
import xyz.thecodeside.tennisapp.remote.RemoteDataSource
import javax.inject.Inject

class GetTennisCourtsUseCase
@Inject constructor(
        private val api: RemoteDataSource,
        private val placesUrlProvider: PlacesUrlProvider
) {

    fun get(query: String?): Single<PlacesResponse> =
            api.getTennisCourts(placesUrlProvider.get(query))
}

