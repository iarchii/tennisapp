package xyz.thecodeside.tennisapp.map

class PlacesLoadingException(errorMessage: String?) : IllegalArgumentException(errorMessage)