package xyz.thecodeside.tennisapp.map

import android.os.Bundle
import android.view.inputmethod.EditorInfo
import android.widget.LinearLayout
import android.widget.TextView
import com.google.android.gms.location.places.GeoDataClient
import com.google.android.gms.location.places.Places
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.LceViewState
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.data.RetainingLceViewState
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_map.*
import xyz.thecodeside.tennisapp.R
import xyz.thecodeside.tennisapp.base.BaseLceActivity
import xyz.thecodeside.tennisapp.helpers.common.hide
import xyz.thecodeside.tennisapp.helpers.common.show
import xyz.thecodeside.tennisapp.map.MapPresenter.View
import xyz.thecodeside.tennisapp.model.PlacesData
import javax.inject.Inject


class MapActivity :
        BaseLceActivity<LinearLayout, List<PlacesData>, View, MapPresenter>(),
        View,
        OnMapReadyCallback {

    private val DEFAULT_MAP_ZOOM = 11.0f

    private var mMap: GoogleMap? = null
    private var mGeoDataClient: GeoDataClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        initToolbar()
        initMap()
        initPlaces()
        initListeners()
    }



    private fun initPlaces() {
        mGeoDataClient = Places.getGeoDataClient(this, null);
    }

    private fun initListeners() {
        searchEt.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                presenter.loadData(v.text.toString())
                return@OnEditorActionListener true
            }
            false
        })

    }

    @Inject lateinit var mapPresenter: MapPresenter

    override fun injectDependencies() {
        super.injectDependencies()
        AndroidInjection.inject(this)
    }

    override fun loadData(pullToRefresh: Boolean) {
        presenter.loadData()
    }

    override fun getData(): List<PlacesData> = presenter.places

    override fun getErrorMessage(e: Throwable?, pullToRefresh: Boolean): String =
            e?.message ?: getString(R.string.unknown_error)

    override fun createPresenter(): MapPresenter = mapPresenter

    override fun setData(data: List<PlacesData>) {
        presenter.setData(data)

    }

    override fun createViewState(): LceViewState<List<PlacesData>, View> =
            RetainingLceViewState<List<PlacesData>, View>()

    override fun showContent() {
        if (data.isEmpty()) {
            emptyView.show()
        } else {
            emptyView.hide()
            showPlaces()
        }
        super.showContent()
    }

    private fun showPlaces() {

        data.forEach {
            val position = LatLng(it.geometry.location.lat,it.geometry.location.lng)
            val markerOptions = MarkerOptions()
                    .position(position)
                    .title(it.name)
            val marker = mMap?.addMarker(markerOptions)
            marker?.tag = it.id
        }
    }

    private fun initMap() {
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        setZoom()
        setCamera()
    }

    private fun setCamera() {
        val base = LatLng(presenter.BASE_LAT, presenter.BASE_LNG)
        mMap?.moveCamera(CameraUpdateFactory.newLatLng(base))
    }

    private fun setZoom() {
        mMap?.setMaxZoomPreference(DEFAULT_MAP_ZOOM)
        mMap?.setMinZoomPreference(DEFAULT_MAP_ZOOM)
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    override fun clear() {
        mMap?.clear()
    }
}

