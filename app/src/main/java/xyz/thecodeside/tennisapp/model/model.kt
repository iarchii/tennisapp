package xyz.thecodeside.tennisapp.model
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlacesResponse (
		@SerializedName("html_attributions") val htmlAttributions: List<String>,
		@SerializedName("next_page_token") val nextPageToken: String, //CpQCAgEAAFxg8o-eU7_uKn7Yqjana-HQIx1hr5BrT4zBaEko29ANsXtp9mrqN0yrKWhf-y2PUpHRLQb1GT-mtxNcXou8TwkXhi1Jbk-ReY7oulyuvKSQrw1lgJElggGlo0d6indiH1U-tDwquw4tU_UXoQ_sj8OBo8XBUuWjuuFShqmLMP-0W59Vr6CaXdLrF8M3wFR4dUUhSf5UC4QCLaOMVP92lyh0OdtF_m_9Dt7lz-Wniod9zDrHeDsz_by570K3jL1VuDKTl_U1cJ0mzz_zDHGfOUf7VU1kVIs1WnM9SGvnm8YZURLTtMLMWx8-doGUE56Af_VfKjGDYW361OOIj9GmkyCFtaoCmTMIr5kgyeUSnB-IEhDlzujVrV6O9Mt7N4DagR6RGhT3g1viYLS4kO5YindU6dm3GIof1Q
		@SerializedName("results") val places: List<PlacesData>?,
		@SerializedName("status") val status: PlacesResponseStatus, //OK
		@SerializedName("error_message") val errorMessage: String?

): Parcelable

enum class PlacesResponseStatus {
    OK,
    ZERO_RESULTS,
    OVER_QUERY_LIMIT,
    REQUEST_DENIED,
    INVALID_REQUEST
}
@Parcelize
data class PlacesData(
		@SerializedName("geometry") val geometry: Geometry,
		@SerializedName("icon") val icon: String, //http://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png
		@SerializedName("id") val id: String, //7eaf747a3f6dc078868cd65efc8d3bc62fff77d7
		@SerializedName("name") val name: String, //Biaggio Cafe - Pyrmont
		@SerializedName("opening_hours") val openingHours: OpeningHours,
		@SerializedName("photos") val photos: List<Photo>,
		@SerializedName("place_id") val placeId: String, //ChIJIfBAsjeuEmsRdgu9Pl1Ps48
		@SerializedName("scope") val scope: String, //GOOGLE
		@SerializedName("price_level") val priceLevel: Int, //1
		@SerializedName("rating") val rating: Double, //3.4
		@SerializedName("reference") val reference: String, //CoQBeAAAAGu0wNJjuZ40DMrRe3mpn7fhlfIK1mf_ce5hgkhfM79u-lqy0G2mnmcueTq2JGWu9wsgS1ctZDHTY_pcqFFJyQNV2P-kdhoRIeYRHeDfbWtIwr3RgFf2zzFBXHgNjSq-PSzX_OU6OT2_3dzdhhpV-bPezomtrarW4DsGl9uh773yEhDJT6R3V8Fyvl_xeE761DTCGhT1jJ3floFI5_c-bHgGLVwH1g-cbQ
		@SerializedName("types") val types: List<String>,
		@SerializedName("vicinity") val vicinity: String //48 Pirrama Rd Pyrmont
) : Parcelable

@Parcelize
data class Photo(
		@SerializedName("height") val height: Int, //600
		@SerializedName("html_attributions") val htmlAttributions: List<String>,
		@SerializedName("photo_reference") val photoReference: String, //CnRnAAAAmWmj0BqA0Jorm1_vjAvx1n6c7ZNBxyY-U9x99-oNyOxvMjDlo2npJzyIq7c3EK1YyoNXdMFDcRPzwLJtBzXAwCUFDGo_RtLRGBPJTA2CoerPdC5yvT2SjfDwH4bFf5MrznB0_YWa4Y2Qo7ABtAxgeBIQv46sGBwVNJQDI36Wd3PFYBoUTlVXa0wn-zRITjGp0zLEBh8oIBE
		@SerializedName("width") val width: Int //900
) : Parcelable

@Parcelize
data class Geometry(
		@SerializedName("location") val location: Location
) : Parcelable

@Parcelize
data class Location(
		@SerializedName("lat") val lat: Double, //-33.867217
		@SerializedName("lng") val lng: Double //151.195939
) : Parcelable

@Parcelize
data class OpeningHours(
		@SerializedName("open_now") val openNow: Boolean //true
) : Parcelable